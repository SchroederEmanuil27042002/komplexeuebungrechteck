package model;

public class Punkt {
	
	private	int x;
	private int y;
	public int getX() {
		return x;
	}
	public void setX(int x) {
		this.x = x;
	}
	public int getY() {
		return y;
	}
	public void setY(int y) {
		this.y = y;
	}
	public Punkt(int x, int y) {
		super();
		this.x = x;
		this.y = y;
	}
	public Punkt() {
		this.x = 0;
		this.y = 0;
	}
	
	public boolean equals(Punkt p) {
		if(p.getX() == this.getX() && p.getY() == this.getY()) {
            return true;
        } else
        return false;
	};
	
	public String toString() {
		return "Punkt [x=" + this.getX() + ", y=" + this.getY() + "]";
	};
	
}
