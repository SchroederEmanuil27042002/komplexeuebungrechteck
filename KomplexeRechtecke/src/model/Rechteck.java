package model;

import java.util.Random;

public class Rechteck {

	private Punkt punkt;
	private int breite;
	private int hoehe;

	public Rechteck() {
		super();
		this.punkt = new Punkt();
		this.breite = 0;
		this.hoehe = 0;
	}

	public Rechteck(int x, int y, int breite, int hoehe) {
		super();
		this.punkt = new Punkt(x, y);
		this.setBreite(breite);
		this.setHoehe(hoehe);
	}

	public int getX() {
		return this.punkt.getX();
	}

	public void setX(int x) {
		this.punkt.setX(x);
	}

	public int getY() {
		return this.punkt.getY();
	}

	public void setY(int y) {
		this.punkt.setY(y);
	}

	public int getBreite() {
		return breite;
	}

	public void setBreite(int breite) {
		this.breite = Math.abs(breite);
	}

	public int getHoehe() {
		return hoehe;
	}

	public void setHoehe(int hoehe) {
		this.hoehe = Math.abs(hoehe);
	}

	@Override
	public String toString() {
		return "Rechteck [x=" + this.getX() + ", y=" + this.getY() + ", breite=" + this.breite + ", hoehe=" + this.hoehe
				+ "]";
	}

	public boolean enthaelt(int x, int y) {
		if ((x >= this.punkt.getX() && x <= (this.punkt.getX() + this.getBreite()))
				&& (y >= this.punkt.getY() && y <= (this.punkt.getY() + this.getHoehe())))
			return true;
		else
			return false;
	}

	public boolean enthealt(Punkt punkt) {
		return enthaelt(punkt.getX(), punkt.getY());
	}

	public boolean enthaelt(Rechteck r) {
		if (this.enthaelt(r.getX(), r.getY()) && this.enthaelt(r.getX() + r.getBreite(), r.getY() + r.getHoehe()))
			return true;
		else
			return false;
	}

	public static Rechteck generiereZufallsRechteck() {
		Random rand = new Random();
		int rechteckSize = 1200;
		int randX = rand.nextInt(rechteckSize);
		int randY = rand.nextInt(rechteckSize);
		Rechteck r = new Rechteck(randX, randY, rand.nextInt(rechteckSize - randX), rand.nextInt(rechteckSize - randY));
		return r;
	}
}