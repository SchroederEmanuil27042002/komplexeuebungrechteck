package view;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;

import controller.BunteRechteckeController;

public class Zeichenflaeche extends JPanel {
	private BunteRechteckeController controller;

	public Zeichenflaeche(BunteRechteckeController controller) {
		this.controller = controller;
	}

	@Override
	public void paintComponent(Graphics g) {
		g.setColor(Color.BLACK);
		for (int i = 0; i < controller.getRechtecke().size(); i++) {
			g.drawRect(controller.rechtecke.get(i).getX(), controller.rechtecke.get(i).getY(),
					controller.rechtecke.get(i).getBreite(), controller.rechtecke.get(i).getHoehe());
		}

	}
}